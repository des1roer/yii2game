<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%pers_unit}}".
 *
 * @property integer $pers_id
 * @property integer $unit_id
 *
 * @property Pers $pers
 * @property Unit $unit
 */
class PersUnit extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pers_unit}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pers_id', 'unit_id'], 'required'],
            [['pers_id', 'unit_id'], 'integer'],
            [['pers_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pers::className(), 'targetAttribute' => ['pers_id' => 'id']],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => Unit::className(), 'targetAttribute' => ['unit_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pers_id' => 'Pers ID',
            'unit_id' => 'Unit ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPers()
    {
        return $this->hasOne(Pers::className(), ['id' => 'pers_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(Unit::className(), ['id' => 'unit_id']);
    }

    /**
     * @inheritdoc
     * @return PersUnitQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PersUnitQuery(get_called_class());
    }
}
