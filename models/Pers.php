<?php

namespace app\models;

/**
 * This is the model class for table "{{%pers}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $lvl
 *
 * @property UserPers[] $userPers
 */
class Pers extends BaseModel
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%pers}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['lvl'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['lvl', 'default', 'value' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'lvl' => 'Lvl',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPers()
    {
        return $this->hasMany(UserPers::className(), ['pers_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return PersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PersQuery(get_called_class());
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnits()
    {
        return $this->hasMany(Unit::className(), ['id' => 'unit_id'])
            ->viaTable('{{%pers_unit}}', ['pers_id' => 'id']);
    }
}
