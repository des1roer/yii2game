<?php

namespace app\models;

/**
 * This is the model class for table "{{%user_pers}}".
 *
 * @property integer $user_id
 * @property integer $pers_id
 *
 * @property User $user
 * @property Pers $pers
 */
class UserPers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user_pers}}';
    }

    public static function primaryKey()
    {
        return [
            'user_id',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'pers_id'], 'required'],
            [['user_id', 'pers_id'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['pers_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pers::className(), 'targetAttribute' => ['pers_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'pers_id' => 'Pers ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPers()
    {
        return $this->hasOne(Pers::className(), ['id' => 'pers_id']);
    }
}
