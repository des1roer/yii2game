<?php

namespace app\models;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

class User extends  \amnah\yii2\user\models\User
{
    public $persona;

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPers()
    {
        return $this->hasMany(Pers::className(), ['id' => 'pers_id'])
            ->viaTable('{{%user_pers}}', ['user_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getPerson(): string
    {
        $pers = $this->pers;
        $pers_ = [];
        foreach ($pers as $v){
            $pers_[] = Html::a($v->name, Url::to(['/pers/view', 'id' => $v->id]), ['class' => 'btn btn-link']);
        }

        return ($pers_) ? implode($pers_) : '';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),
            [ 'persona', 'safe']);

    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),
            [ 'persona' => 'Pers']);
    }
}
