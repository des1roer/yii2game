<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PersUnit]].
 *
 * @see PersUnit
 */
class PersUnitQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PersUnit[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PersUnit|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
