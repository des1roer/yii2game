Yii 2 Game
============================

1. Скопировать local.example как local и прописать свои настройки
2. Сделать composer update
3. Применить миграции
    - yii migrate
    
============================

Ипсользование миграций https://github.com/tmukherjee13/yii2-reverse-migration

for table migration,

    yii migration/table <tablename>
or

    yii migration/table <tablename1>,<tablename2>

for data migration,

    yii migration/data <tablename>
or

    yii migration/data <tablename1>,<tablename2>

to create migration of whole schema,

    yii migration/schema <schemaname>

===========================

Создание модели в консоли 

    yii gii/model --tableName=ds_unit --modelClass=Unit --generateQuery=1 --useTablePrefix=1
    
    yii gii/crud --modelClass=app\models\Unit --controllerClass=app\controllers\UnitController --enablePjax=1 --searchModelClass=app\models\UnitSearch
    
    
-----------------------------------------
https://github.com/krivochenko/yii2-users


----------------------------------

Прототипирование миграций 

     yii migration/table user_pers --db=db_dev
     
     yii migration/up