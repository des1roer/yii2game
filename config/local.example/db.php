<?php

return [
    'db' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=localdb',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'tablePrefix' => 'ds_',
    ],
    'db_dev' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=localhost;dbname=dev',
        'username' => 'root',
        'password' => '',
        'charset' => 'utf8',
        'tablePrefix' => '',
    ]
];
