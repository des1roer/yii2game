<?php

namespace app\controllers;

use app\models\Pers;
use yii\rest\ActiveController;

class MyController extends ActiveController
{
    public $modelClass = Pers::class;
}
