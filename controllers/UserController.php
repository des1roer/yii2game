<?php
namespace app\controllers;

use amnah\yii2\user\controllers\AdminController;
use Yii;

class UserController extends AdminController
{
    public function actionIndex()
    {
        /** @var \amnah\yii2\user\models\search\UserSearch $searchModel */
        $searchModel = $this->module->model("UserSearch");
        $dataProvider = $searchModel->search(Yii::$app->request->getQueryParams());

        return $this->render('index', compact('searchModel', 'dataProvider'));
    }
}