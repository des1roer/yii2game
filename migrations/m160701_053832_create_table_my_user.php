<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%my_user}}`.
 */
class m160701_053832_create_table_my_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%my_user}}', [

            'id' => $this->integer(11)->notNull(),

        ]);
 
        // creates index for column `id`
        $this->createIndex(
            'my_user_fk1',
            '{{%my_user}}',
            'id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'my_user_fk1',
            '{{%my_user}}',
            'id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'my_user_fk1',
            '{{%my_user}}'
        );

        // drops index for column `id`
        $this->dropIndex(
            'my_user_fk1',
            '{{%my_user}}'
        );

        $this->dropTable('{{%my_user}}');
    }
}
