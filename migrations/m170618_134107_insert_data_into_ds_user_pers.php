<?php

use yii\db\Migration;

/**
 * Handles the data insertion for table `{{%user_pers}}`.
 */
class m170618_134107_insert_data_into_ds_user_pers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->batchInsert('{{%my_user}}', ['id'], [ ['1'] ]);
        $this->batchInsert('{{%user_pers}}', ['user_id','pers_id'], [ ['1','1'] ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->truncateTable('{{%user_pers}}');
        $this->delete('{{%my_user}}');
    }
}
