<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%user_pers}}`.
 */
class m170618_080116_create_table_user_pers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%user_pers}}', [

            'user_id' => $this->integer(11)->notNull(),
            'pers_id' => $this->integer(10)->unsigned()->notNull(),

        ]);
 
        // creates index for column `user_id`
        $this->createIndex(
            'user_pers_fk1',
            '{{%user_pers}}',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'user_pers_fk1',
            '{{%user_pers}}',
            'user_id',
            '{{%my_user}}',
            'id',
            'CASCADE'
        );

        // creates index for column `pers_id`
        $this->createIndex(
            'user_pers_fk2',
            '{{%user_pers}}',
            'pers_id'
        );

        // add foreign key for table `pers`
        $this->addForeignKey(
            'user_pers_fk2',
            '{{%user_pers}}',
            'pers_id',
            '{{%pers}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'user_pers_fk1',
            '{{%user_pers}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'user_pers_fk1',
            '{{%user_pers}}'
        );

        // drops foreign key for table `pers`
        $this->dropForeignKey(
            'user_pers_fk2',
            '{{%user_pers}}'
        );

        // drops index for column `pers_id`
        $this->dropIndex(
            'user_pers_fk2',
            '{{%user_pers}}'
        );

        $this->dropTable('{{%user_pers}}');
    }
}
