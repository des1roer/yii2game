<?php

use yii\db\Migration;

/**
 * Handles the data insertion for table `{{%pers}}`.
 */
class m170618_134014_insert_data_into_ds_pers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->batchInsert('{{%pers}}', ['id','name','lvl'], [
			['1','Test','1']]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->delete('{{%pers}}');
    }
}
