<?php

use yii\db\Migration;

/**
 * Handles the data insertion for table `{{%pers_unit}}`.
 */
class m170701_071852_insert_data_into_ds_pers_unit extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->batchInsert('{{%pers_unit}}', ['pers_id','unit_id'], [
			['1','1']]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->truncateTable('{{%pers_unit}}');
    }
}
