<?php

use yii\db\Migration;

/**
 * Handles the creation for table `{{%pers}}`.
 */
class m170618_075159_create_table_pers extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('{{%pers}}', [

            'id' => $this->primaryKey()->unsigned()->notNull(),
            'name' => $this->string(255)->notNull(),
            'lvl' => $this->integer(10)->unsigned()->notNull()->defaultValue(1),

        ]);
     }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%pers}}');
    }
}
