<?php

namespace app\modules\api\modules\v1\controllers;

use app\models\Unit;
use yii\data\ActiveDataProvider;

/**
 * Class UnitController
 * @link http://yii2game/api/unit
 */
class UnitController extends RestController
{
    public $modelClass = Unit::class;

    public function actionCustom(?int $id = null): ActiveDataProvider
    {
        $query = Unit::find();
        if ($id) {
            $query->andWhere(['id' => $id]);
        }

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }
}
