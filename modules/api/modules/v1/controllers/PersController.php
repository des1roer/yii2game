<?php

namespace app\modules\api\modules\v1\controllers;

use app\models\Pers;

/**
 * Class PersController
 * @link http://yii2game/api/pers
 */
class PersController extends RestController
{
    public $modelClass = Pers::class;
}
