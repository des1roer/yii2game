<?php

namespace app\modules\admin\models;

use app\models\UserPers;
use amnah\yii2\user\models\User;
use app\models\Pers;

/**
 * This is the model class for table "{{%my_user}}".
 *
 * @property integer $id
 *
 * @property User $id0
 * @property UserPers[] $userPers
 */
class MyUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%my_user}}';
    }

    public static function primaryKey()
    {
        return [
            'id',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id'], 'integer'],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPers()
    {
        return $this->hasMany(Pers::className(), ['id' => 'pers_id'])
            ->viaTable('{{%user_pers}}', ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPers()
    {
        return $this->hasMany(UserPers::className(), ['user_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserPerson()
    {
        return UserPers::findOne($this->id);
    }

    /**
     * @inheritdoc
     * @return MyUserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MyUserQuery(get_called_class());
    }
}
